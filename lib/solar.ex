defmodule Solar do
  def power(%{classification: :M, scale: s}),
    do: s * 10
  def power(%{classification: :X, scale: s, stations: c}) when c < 5,
    do: s * 1000 * 1.1
  def power(%{classification: :X, scale: s, stations: c}) when c >= 5,
    do: s * 1000
  def power(%{classification: :C, scale: s}), do: s

  def no_eva(flares), do: Enum.filter flares, &(power(&1) > 1000)

  def deadliest(flares), do: Enum.map(flares, &(power(&1))) |> Enum.max

  def flare_list(flares) do
    Enum.map flares, fn(flare) ->
      p = power(flare)
      %{power: p, is_deadly: p > 1000}
    end
  end

  def total_flare_power(list), do: total_flare_power(list, 0)
  def total_flare_power([], total), do: total

  def total_flare_power([%{classification: :M} = head | tail], total) do
    new_total = power(head) * 0.92 + total
    total_flare_power(tail, new_total)
  end
  def total_flare_power([%{classification: :C} = head | tail], total) do
    new_total = power(head) * 0.78 + total
    total_flare_power(tail, new_total)
  end
  def total_flare_power([%{classification: :X} = head | tail], total) do
    new_total = power(head) * 0.68 + total
    total_flare_power(tail, new_total)
  end

end