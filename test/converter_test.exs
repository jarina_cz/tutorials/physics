defmodule ConverterTest do
  use ExUnit.Case

  test "Converting to to_light_seconds" do
    ls = Converter.to_light_seconds({:miles, 1000}, precision: 5)
    assert ls == 0.00537
  end

  test "Converting to to_light_seconds with default values" do
    ls = Converter.to_light_seconds({:miles, 1000})
    assert ls == 0.00537
  end

end